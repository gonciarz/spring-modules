package rg.cfrm.reader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import rg.cfrm.common.CommonsConfig;

@Slf4j
@SpringBootApplication
@Import(CommonsConfig.class)
public class ReaderApplication {

	@Autowired
    private rg.cfrm.common.ports.EsConnector connector;

	public static void main(String[] args) {
		SpringApplication.run(ReaderApplication.class, args);
	}

    @Bean
    CommandLineRunner runner() {
        return args -> log.info("Connector: {}", connector.name());
    }


}

