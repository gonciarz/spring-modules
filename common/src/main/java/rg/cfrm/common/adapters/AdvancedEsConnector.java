package rg.cfrm.common.adapters;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import rg.cfrm.common.ports.EsConnector;

@Component
@ConditionalOnProperty(name = "es.connector", havingValue = "advanced")
public class AdvancedEsConnector implements EsConnector {

    @Override
    public String name() {
        return "advanced";
    }
}
