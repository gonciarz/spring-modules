package rg.cfrm.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "rg.cfrm.common")
public class CommonsConfig {

}
